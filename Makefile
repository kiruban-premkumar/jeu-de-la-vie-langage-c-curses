# Program Name
EXEC_GUI = lifeGame_gui
EXEC_TXT = lifeGame_txt 

# Compilation's Options
CFLAGS = -Wall -g

# Linker Options
LD_OPTIONS_GUI = -lm -lcurses
LD_OPTIONS_TXT = -lm

# Source Dir
SRC = ./src/

# Binary Dir
BIN = ./bin/

# Final Objects
OBJS_GUI = ${BIN}main.o ${BIN}userinterface_gui.o ${BIN}jdv.o ${BIN}bmp.o
OBJS_TXT = ${BIN}main.o ${BIN}userinterface_txt.o ${BIN}jdv.o ${BIN}bmp.o

# Compilator
CC = gcc

# Principal rule
all : ${EXEC_GUI} ${EXEC_TXT}

# Rules to construct the final program in GUI MODE
${EXEC_GUI} : ${OBJS_GUI}
	${CC} ${CFLAGS} -o $@ $^ ${LD_OPTIONS_GUI}

# Rules to construct the final program in TXT MODE
${EXEC_TXT} : ${OBJS_TXT}
	${CC} ${CFLAGS} -o $@ $^ ${LD_OPTIONS_TXT}

# Make a bin dir
bin :
	mkdir -p ./bin

# Construct object in the bin dir
${BIN}main.o : ${SRC}main.c bin ${BIN}jdv.o ${BIN}bmp.o
	${CC} ${CFLAGS} -c $<
	mv main.o ${BIN}

${BIN}userinterface_gui.o : ${SRC}userinterface_gui.c ${SRC}config.h ${SRC}jdv.h ${SRC}userinterface.h
	${CC} ${CFLAGS} -c $<
	mv userinterface_gui.o ${BIN}

${BIN}userinterface_txt.o : ${SRC}userinterface_txt.c ${SRC}config.h ${SRC}jdv.h ${SRC}userinterface.h
	${CC} ${CFLAGS} -c $<
	mv userinterface_txt.o ${BIN}

${BIN}jdv.o : ${SRC}jdv.c ${SRC}config.h ${SRC}jdv.h ${SRC}dbg.h
	${CC} ${CFLAGS} -c $<
	mv jdv.o ${BIN}

${BIN}bmp.o : ${SRC}bmp.c ${SRC}config.h ${SRC}jdv.h ${SRC}bmp.h
	${CC} ${CFLAGS} -c $<
	mv bmp.o ${BIN}

# clean objects
clean:
	rm -f ./bin/*.o

# clean all
clean-all: clean
	rm -f ${EXEC_GUI} ${EXEC_TXT}
	rm -rf bin
	rm -f doxygen
	rm -f jdv.bmp