#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "jdv.h"
#include "userinterface.h"

struct _Tui {
    FILE* stream;
};

extern Tui* ui_init(int dx, int dy)
{
    Tui* ret = malloc(sizeof(*ret));
    ret->stream = stdout;
    return ret;
}

extern void ui_itaff(const Tui *ui, int it)
{
    fprintf(ui->stream,"[Generation %d] \n",it);
}

extern void ui_graff(const Tui *ui, const aGrid *grid)
{
	int i,j;
	fprintf(ui->stream,"+");
    for(i=0;i<grid->size_x;i++)
		fprintf(ui->stream,"-");
	fprintf(ui->stream,"+");
	fprintf(ui->stream,"\n");
	for(j=grid->size_y-1;j >= 0;j--)
	{
		fprintf(ui->stream,"|");
		for(i=0;i<grid->size_x;i++)
		{
			if(grid->t[i][j] == 0){
				fprintf(ui->stream,".");
			} else {
				fprintf(ui->stream,"o");
			}
		}
		fprintf(ui->stream,"|\n");
	}
	fprintf(ui->stream,"+");
	for(i=0;i<grid->size_x;i++)
		fprintf(ui->stream,"-");
	fprintf(ui->stream,"+");
	fprintf(ui->stream,"\n");
}

extern void ui_oscilaff(const Tui *ui, int length, int nextGen)
{
	fprintf(ui->stream,"+----------------------+\n");
	fprintf(ui->stream,"| OSCILLATION DETECTED |\n");
	fprintf(ui->stream,"+----------------------+\n");
	fprintf(ui->stream,"\nLENGTH = %d\nNEXT GENERATION => %d\n\n",length,nextGen);
}

extern void ui_timeout(const Tui *ui, int to)
{
	// not needed for txt ui
}

extern void ui_end(Tui *ui)
{
	free(ui);
}