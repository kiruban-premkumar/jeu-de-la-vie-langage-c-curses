#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <string.h>
#include "config.h"
#include "jdv.h"
#include "userinterface.h"

struct _Tui {
    WINDOW* mwin;
};

extern Tui* ui_init(int dx, int dy)
{
    Tui* ret = malloc(sizeof(*ret));
    
    ret->mwin=initscr();
    if(dx >= COLS || dy >= LINES)
    {
        endwin();
        fprintf(stderr,"The grid size is bigger than the screen size, it must be lower than %dx%d in order to be printed in the GUI mode.",(COLS>100?100:COLS)-1,(LINES>100?100:LINES)-1);
        exit(1);
    }

    cbreak();
    noecho();
    curs_set(0);
    keypad(stdscr, TRUE);
    
    start_color();
    short r=0, g=255, b=255;

    init_color(2, r, g, b);
    init_pair(1, COLOR_RED, COLOR_GREEN);
    init_pair(2, COLOR_RED, COLOR_GREEN);
    init_pair(3, 2, 2);
    return ret;
}

extern void ui_itaff(const Tui *ui, int it)
{
    refresh();
    mvprintw(0,0,"[Generation %d]",it); 
}

extern void ui_graff(const Tui *ui, const aGrid *grid)
{
    int i,j,y,x;
    y = grid->size_y;
	for(j=0;j<grid->size_y;j++)
	{
        x = 1;
        for(i=0;i<grid->size_x;i++)
        {
        	if(grid->t[i][j] == 0){
                mvwaddch(ui->mwin, y, x, '.');
        	} else {
                mvwaddch(ui->mwin, y, x, 'o');
        	}
            x++;
        }
        y--;
	}
    refresh();
}

extern void ui_oscilaff(const Tui *ui, int length, int nextGen)
{
    refresh();
    int row,col;
    getmaxyx(stdscr,row,col);
    mvprintw(row/2,(col-24)/2,"+----------------------+");
    mvprintw(row/2+1,(col-24)/2,"| OSCILLATION DETECTED |");
    mvprintw(row/2+2,(col-24)/2,"+----------------------+");
    mvprintw(row/2+3,(col-24)/2,"LENGTH = %d",length);
    mvprintw(row/2+4,(col-24)/2,"NEXT GENERATION => %d",nextGen);
    refresh();
}

extern void ui_timeout(const Tui *ui, int to)
{
    sleep(to);
}

extern void ui_end(Tui *ui)
{
    endwin();
    free(ui);
}