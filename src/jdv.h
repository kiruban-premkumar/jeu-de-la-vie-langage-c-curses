/**
 * \struct _aGrid
 * \brief Structure pour la grille
 * \details _aGrid est une structure qui a un tableau t 
 *					à deux dimentions initialisé par défaut à XMAX et YMAX 
 *					et la taille du tableau lu est stocké dans size_x et size_y.
 */
typedef struct _aGrid {
	int t[XMAX][YMAX]; 	/*!< Grille de celulle. */
	int size_x, size_y; /*!< Taille du tableau (size_x et size_y) lue par fichier,stdin ou option -t. */
} aGrid;

/**
 * \struct _aHisto
 * \brief Structure pour stocker l'historique des grilles.
 * \details _aHisto permet de stocker les grilles pour faire des recherches d'oscillations.
 */
typedef struct _aHisto {
	aGrid t[HMAX+1]; 	/*!< Tableau de grilles. */
	int count; 			/*!< Compteur du nombre de grille stocké. */
} aHisto;

/**
 * \struct _lifeGameConfig
 * \brief Structure pour stocker les options.
 * \details _lifeGameConfig permet de stocker les options : la graine (seed), le pourcentage (percentage) et le nombre de génération (nbGen).
 */
typedef struct _lifeGameConfig {
	int seed;				/*!< La graine pour la génération automatique de la grille */
	int percentage; /*!< Le pourcentage pour la génération automatique de la grille. */
	int nbGen; 			/*!< Le nombre de génération à afficher. */
	int globe;      /*!< Permet de manipuler l'option -f pour considéré la grille de cellule comme un globe. */
	int disable_gui;
} lifeGameConfig;

/**
 * \brief	Fonction d'initialisation principale.
 * \details	Initialise la grille (grid) à des tailles (size_x et size_y) nulles.
 * 					Initialise l'historique (histo) à un compteur (count) nul.
 *					Initialise la configuration (config) => la graine (seed) est initialisé à nulle, 
 *          le pourcentage (percentage) est initialisé à nulle et le nombre de génération (nbGen)
 *					est initialisé à nul. 
 * \param grid La grille à initialiser.
 * \param histo L'historique à initialiser.
 * \param config La configuration à initialiser.
 */
extern void jdv_initLifeGame(aGrid *grid, aHisto *histo, lifeGameConfig *config);

/**
 * \brief	Fonction d'historisation des grilles et détection d'oscillation.
 * \details	Parcourt l'historique (histo) des grilles et sauvegarde la grille grid.
 * \param grid La grille à sauvegarder.
 * \param histo L'historique des grilles.
 * \return int L'indice de l'oscillation : s'il y a une oscillation.
 *						 -1 : s'il n'y a pas d'oscillation.
 */
extern int jdv_logHisto(const aGrid *grid, aHisto *histo);

/**
 * \brief	Fonction calculant la génération suivante.
 * \details	Parcourt chaque cellule et calcule la génération suivante en sauvegardant une grille locale temporaire tmp.
 * \param grid La grille à calculer.
 */
extern void jdv_nextGeneration(aGrid *grid, const lifeGameConfig *config);

/**
 * \brief	Fonction de traitement des options.
 * \details	Parcourt les options et enregistre les options dans config (seed,percentage,nbGen) et grid (size_x,size_y).
 * \param grid La grille pour le stockage des tailles (size_x et size_y).
 * \param config La configuration pour le stockage des options (seed,percentage,nbGen).
 */
extern void jdv_optionsProcessing(aGrid *grid, lifeGameConfig *config, int argc, char **argv);
