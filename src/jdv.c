#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "config.h"
#include "jdv.h"
#include "dbg.h"

/**
 * \brief	Fonction d'initialisation d'une grille.
 * \details	Initialise toutes les valeurs d'une grille grid à 0.
 * \param grid La grille à initialiser.
 */
static void jdv_initGrid(aGrid *grid)
{
	int i,j;
	for(i=0;i<grid->size_x;i++)
		for(j=0;j<grid->size_y;j++)
			grid->t[i][j] = 0;
}

/**
 * \brief	Fonction de copie d'une grille dans une autre grille.
 * \details	Copie une grille tmp dans une grille grid.
 * \param grid La grille destinataire de la copie.
 * \param tmp La grille à copier, la source.
 */
static void jdv_cpyGrid(aGrid *grid, const aGrid *tmp)
{
	int i,j;
	for(i=0;i<grid->size_x;i++)
		for(j=0;j<grid->size_y;j++)
			grid->t[i][j] = tmp->t[i][j];
}

/**
 * \brief	Fonction traitant les données entrant.
 * \details	Lit les données en entrées à partir d'un fichier ou de stdin.
 * \param grid La grille pour le stockage/lecture des tailles (size_x et size_y) et le stockages des coordonnées.
 * \param config La configuration pour le lire les options (seed,percentage,nbGen).
 * \param argv L'affiche des erreurs, nécessaire au macro log_err.
 */
static void jdv_readInput(aGrid *grid, const lifeGameConfig *config, FILE *file, char **argv){
	int k, r;
	k = 0;
	while(!feof(file))
	{      
		if(k == 0)
		{
			if(grid->size_x == 0 || grid->size_y == 0)
			{
				r = fscanf(file,"%d %d",&grid->size_x,&grid->size_y);
				if(r != 2)
				{
	        		log_err("The grid size is incorrect, it must be lower than %d %d and greater than %d %d.",XMAX, YMAX, XMIN, YMIN);
	        		fclose(file);
	        		exit(1);
				}
			}
			if(grid->size_x > XMAX || grid->size_y > YMAX || grid->size_x < XMIN || grid->size_y < YMIN)
			{
				log_err("The grid size is incorrect, it must be lower than %d %d and greater than %d %d.",XMAX, YMAX, XMIN, YMIN);
				fclose(file);
				exit(1);
			}

			jdv_initGrid(grid); // Inizialisation of the grid once size parameters are available

			// If the percentage is defined you don't need to procceed with coordinates so you return the grid.
			if(config->percentage > 0)
				return;
		}	
		else
		{
			int x, y;
			r = fscanf(file,"%d %d",&x,&y);
			if(r == 2)
			{
				if(x < 0 || x > grid->size_x-1 || y < 0 || y > grid->size_y-1)
                {
                    log_err("Coordinates are incorrect or it exceeds the grid size.");
                    fclose(file);
                    exit(1);
                }
                else
                {
          	        grid->t[x][y] = 1;
                }
			}
		}	
		k++;
	}
	fclose(file);
	return;
}

/**
 * \brief	Fonction de comparaison de deux grilles.
 * \details	Parcourt et compare deux grilles gridA et gridB.
 * \param gridA La grille A à comparer.
 * \param gridB La grille B à comparer.
 * \return int 0 : si les grilles sont identiques.
 *             -1 : si les grilles ne sont pas identiques.
 */
static int jdv_cmpGrid(const aGrid *gridA, const aGrid *gridB)
{
	int i, j;

	for(i=0;i<gridA->size_x;i++)
		for(j=0;j<gridA->size_y;j++)
			if(gridA->t[i][j] != gridB->t[i][j]) return -1;

	return 0;
}

/**
 * \brief	Fonction de génération de grille.
 * \details	Génére une grille grid à partir d'une configuration config.
 * \param grid La grille à générer.
 * \param config La configuration du jeux de la vie dont dépend la génération.
 */
static void jdv_generateGrid(aGrid *grid, const lifeGameConfig *config)
{
	int nbAlive = floor(grid->size_x * grid->size_y * config->percentage / 100);
	int nbCurrent = 0;
	int i,j,tmp;

	if(config->seed > 0)
		srand(config->seed);
	else
		srand((unsigned)time(NULL));

	while(nbCurrent < nbAlive) {
		for( i = 0; i < grid->size_x; i++ ){
			for( j = 0; j < grid->size_y; j++ ){
				if(nbCurrent == nbAlive) return;
				tmp = rand()%2;
				if(tmp == 1 && grid->t[i][j] == 0)
				{
					grid->t[i][j] = 1;
					nbCurrent++;
				}
			}
		}
	}
}

/**
 * \brief	Fonction cherchant le nombre de voisin dans un globe.
 * \details	Cherche le nombre de voisin pour une cellule(i,j) dans une grille (grid) considéré comme un globe.
 * \param i indice x de la cellule dont on veut le nombre de voisin.
 * \param j indice y de la cellule dont on veut le nombre de voisin.
 * \return int Le nombre de voisins.
 */
static int jdv_findNeighborsOfTheGlobe(const aGrid *grid, const int i, const int j)
{
	int n = 0;
	int ofx,ofy;
	int x0,y0;

	for(ofx=-1;ofx<=1;ofx++)
	{
		for(ofy=-1;ofy<=1;ofy++)
		{
			x0=i+ofx%grid->size_x;
			y0=j+ofy%grid->size_y;
			if(x0<0) x0 += grid->size_x;
			if(y0<0) y0 += grid->size_y;
			if(x0 < grid->size_x && y0 < grid->size_y) n += grid->t[x0][y0];
		}
	}
	n -= grid->t[i][j];
	return n;
}

/**
 * \brief	Fonction cherchant le nombre de voisin dans un tableau.
 * \details	Cherche le nombre de voisin pour une cellule(i,j) dans une grille (grid) comme un tableau.
 * \param i indice x de la cellule dont on veut le nombre de voisin.
 * \param j indice y de la cellule dont on veut le nombre de voisin.
 * \return int Le nombre de voisins.
 */
static int jdv_findNeighborsOfTheTable(const aGrid *grid, const int i, const int j)
{
	int n = 0;
	if(i+1 < grid->size_x && grid->t[i+1][j] == 1)
		n++;
	if(i-1 >= 0 && grid->t[i-1][j] == 1)
		n++;
	if(j+1 < grid->size_y && grid->t[i][j+1] == 1)
		n++;
	if(j-1 >= 0 && grid->t[i][j-1] == 1)
		n++;
	if(i+1 < grid->size_x && j+1 < grid->size_y && grid->t[i+1][j+1] == 1)
		n++;
	if(i-1 >= 0 && j-1 >= 0 && grid->t[i-1][j-1] == 1)
		n++;
	if(i+1 < grid->size_x && j-1 >= 0 && grid->t[i+1][j-1] == 1)
		n++;
	if(i-1 >= 0 && j+1 < grid->size_y && grid->t[i-1][j+1] == 1)
		n++;
	return n;
}

extern void jdv_initLifeGame(aGrid *grid, aHisto *histo, lifeGameConfig *config)
{
	grid->size_x = 0;
	grid->size_y = 0;
	histo->count = 0;
	config->nbGen = 0;
	config->seed = 0;
	config->percentage = 0;
	config->globe = 0;
	config->disable_gui = 0;
}

extern int jdv_logHisto(const aGrid *grid, aHisto *histo)
{	
	int i;
	for(i=histo->count-1; i >= 0; i--)
	{
	  if(jdv_cmpGrid(grid,&histo->t[i]) == 0)
	    return i;
	}
	histo->t[histo->count].size_x = grid->size_x;
	histo->t[histo->count].size_y = grid->size_y;
	jdv_initGrid(&histo->t[histo->count]);
	jdv_cpyGrid(&histo->t[histo->count],grid);
	histo->count++;
	return -1;
}

extern void jdv_nextGeneration(aGrid *grid, const lifeGameConfig *config)
{
	int i,j,nb;
	aGrid *tmp = malloc(sizeof(aGrid));
	
	tmp->size_x = grid->size_x;
	tmp->size_y = grid->size_y;
	jdv_initGrid(tmp);
	
	for(i=0;i<grid->size_x;i++)
	{
		for(j=0;j<grid->size_y;j++)
		{
			
			if(config->globe == 1)
				nb = jdv_findNeighborsOfTheGlobe(grid,i,j);
			else
				nb = jdv_findNeighborsOfTheTable(grid,i,j);

			if(grid->t[i][j] == 0 && nb == 3) 
				tmp->t[i][j] = 1;
			if(grid->t[i][j] == 1 && (nb == 0 || nb == 1)) 
				tmp->t[i][j] = 0;
			if(grid->t[i][j] == 1 && (nb == 2 || nb == 3)) 
				tmp->t[i][j] = 1;
			if(grid->t[i][j] == 1 && nb >= 4 && nb <= 8) 
				tmp->t[i][j] = 0;
		}
	}
	jdv_cpyGrid(grid,tmp);
	free(tmp);
}

extern void jdv_optionsProcessing(aGrid *grid, lifeGameConfig *config, int argc, char **argv)
{
    int opt,i;
    FILE *file = NULL;
    FILE *tmp = NULL;

    for(i = 1; i < argc; i++)
	{
		tmp = fopen(argv[i],"r");
		if(tmp != NULL)
		{
			file = fopen(argv[i],"r"); 
			if(i == 1)
			{
				optind = 2;
			}
		}
	}

	static struct option long_options[] = {
        {"file", 1, 0, 0}
    };
    int option_index = 0;

    while((opt = getopt_long(argc,argv,"dfhg:t:r:G:",long_options, &option_index)) != -1)
    {
        switch(opt)
        {
			case 'd':
				config->disable_gui = 1;
				break;
        	case 'f':
        		config->globe = 1;
        		break;
            case 'g':
				config->nbGen = atoi(optarg);
            	break;
            case 'G':
            	config->seed = atoi(optarg);
            	break;
            case 't':
                if(!argv[optind-1] || !argv[optind])
                {
                  	log_err("-t option must have two value, for example -t size_x size_y");
                	exit(1);
                }
                grid->size_x = atoi(argv[optind-1]);
                grid->size_y = atoi(argv[optind]);
                optind = optind+1;
            	break;
            case 'r':
                config->percentage = atoi(optarg);
                break;
            case 'h':
                printf("[PROGRAM]\n");
                printf("%s\n",argv[0]);
                printf("        This programme generate grids of cellulars evolutions from its initial setup.\n");
                printf("[OPTIONS]\n");
				printf("-d\n");
                printf("        Set -d without any value in order to disable the User Interface and make a BMP output.\n");
                printf("-f\n");
                printf("        Set -f without any value in order to make the grid to behave like a globe.\n");
                printf("-g num\n");
                printf("        Set a default number of generation to show.\n");
                printf("-r num\n");
                printf("        Set a percentage from 1 to 100 in order to generate the grid automatically.\n");
                printf("-G num\n");
                printf("        Set -G to use a seed value for the generation.\n");
                printf("-t size_x size_y\n");
                printf("        Set -t followed by two values in order to set the size of the grid.\n");
                exit(0);
            default:
            	break;
        }

    }

    if(file == NULL)
    	file = stdin;

    if(config->nbGen > HMAX)
    {
        log_err("The number of generation to show can't be greater than %d.",HMAX);
        fclose(file); exit(1);
    }

    if(config->nbGen == 0)
    	config->nbGen = N;
    if(grid->size_x > 0 && grid->size_y > 0 && config->percentage > 0)
    {
    	if(grid->size_x > XMAX || grid->size_y > YMAX){
    		log_err("The grid size is incorrect, it must be lower than %d %d and greater than %d %d.",XMAX, YMAX, XMIN, YMIN);
    		fclose(file); exit(1);
    	}
    	if(config->percentage > 100)
    	{
    		log_err("The percentage can't be greater than 100.");
    		fclose(file); exit(1);
    	}
    	fclose(file);
    	jdv_generateGrid(grid,config);
    	return;
    }
    jdv_readInput(grid,config,file,argv);
       
    if(config->percentage > 0)
    	jdv_generateGrid(grid,config);
    
    return;
}
