/**
 * \brief	Fonction d'affichage d'une grille.
 * \details	Affichage d'une grille grid en respectant l'axe des x et y.
 * \param grid La grille à afficher.
 */

typedef struct _Tui Tui;
extern Tui* ui_init(int dx, int dy);
extern void ui_itaff(const Tui *ui, int it);
extern void ui_graff(const Tui *ui, const aGrid *grid);
extern void ui_oscilaff(const Tui *ui, int length, int nextGen);
extern void ui_timeout(const Tui *ui, int to);
extern void ui_end(Tui *ui);