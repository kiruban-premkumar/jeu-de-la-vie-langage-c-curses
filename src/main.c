/**
 * \file main.c
 * \brief Life Game Program.
 * \author Kiruban PREMKUMAR & Sofiane Ben Bourahel
 * \version 3.1
 * \date December 14, 2012
 *
 * Life game program generates N number of generations from an initial generation.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "jdv.h"
#include "userinterface.h"
#include "bmp.h"

int main(int argc, char **argv){
	int r;
	int g = 0;
	aGrid grid;
	aHisto histo;
	lifeGameConfig config;

	jdv_initLifeGame(&grid,&histo,&config);

	jdv_optionsProcessing(&grid,&config,argc,argv);

	if(config.disable_gui == 1)
	{
		// START BITMAP PROCESSSING
		bmp_output(&grid,&histo,&config);
		return 0;	
		// END OF BITMAP PROCESSING
	}
    
    Tui* ui;
    ui = ui_init(grid.size_x,grid.size_y);
    // START OF NORMAL PROCESSING, GUI OR TXT DEPENDS ON APPLICATION
    ui_itaff(ui,g);
    ui_graff(ui,&grid);
    ui_timeout(ui,1);

	jdv_logHisto(&grid,&histo);
	jdv_nextGeneration(&grid,&config);

	g = 1;
	while(g <= config.nbGen)
	{
	    ui_itaff(ui,g);
	    ui_graff(ui,&grid);
	    ui_timeout(ui,1);

	  	r = jdv_logHisto(&grid,&histo);
	    if(r != -1)
	    {
	      	ui_oscilaff(ui,histo.count-r,histo.count+1);
	      	ui_timeout(ui,5);
	 		break;
	    }
	  	jdv_nextGeneration(&grid,&config);
		g++;
	}
	ui_timeout(ui,5);
	ui_end(ui);
	return 0;
}
