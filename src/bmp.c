#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "config.h"
#include "jdv.h"
#include "bmp.h"
#include <arpa/inet.h>

extern void bmp_output(aGrid *grid, aHisto *histo, const lifeGameConfig *config)
{
	if(config->nbGen > 99)
	{
		fprintf(stderr, "I can't output BMP Image File, for more than 99 generations.... SORRY.\n");
		exit(1);
	}
	int g = 0;
	// Image properties
	int x,y,w,h;
	w = 1000;
	//printf("width = %d, g = %d\n", w, config->nbGen);
	h = ceil((config->nbGen+1)/10.0)*grid->size_y;
	unsigned char p[h][w][3];

	// Bitmap structures to be written to file
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;

	// Fill BITMAPFILEHEADER structure
	memcpy((char *)&bfh.bfType, "BM", 2);
	bfh.bfSize = sizeof(bfh) + sizeof(bih) + 3*h*w;
	bfh.bfReserved1 = 0;
	bfh.bfReserved2 = 0;
	bfh.bfOffBits = sizeof(bfh) + sizeof(bih);

	// Fill BITMAPINFOHEADER structure
	bih.biSize = sizeof(bih);
	bih.biWidth = w;
	bih.biHeight = h;
	bih.biPlanes = 1;
	bih.biBitCount = 24;
    bih.biCompression = 0;
	bih.biSizeImage = 0; // can be zero for BI_RGB bitmaps
	bih.biXPelsPerMeter = 0;
	bih.biYPelsPerMeter = 0;
	bih.biClrUsed = 0;
	bih.biClrImportant = 0;

	FILE *f;
	f = fopen("jdv.bmp", "wb");

	// Write bitmap file header
	fwrite(&bfh, 1, sizeof(bfh), f);
	fwrite(&bih, 1, sizeof(bih), f);

	// init all the pixel to black
	for (y=h-1 ; y>=0 ; --y)
	{
		for (x=0 ; x<w ; ++x)
		{
			p[y][x][0] = 0;
            p[y][x][1] = 0;
			p[y][x][2] = 0;
		}
	}
	
	// WRITE FIRST GENERATION HERE
	// Generate pixel data
	for (y=0 ; y<grid->size_y ; ++y)
	{
		for (x=0 ; x<grid->size_x ; ++x)
		{
			if(grid->t[x][y] == 1)
			{
				p[y][x][0] = 0;
            	p[y][x][1] = 0;
				p[y][x][2] = 255;
			}
			else
			{
				p[y][x][0] = 0;
            	p[y][x][1] = 0;
				p[y][x][2] = 0;
			}
		}
	}

	jdv_nextGeneration(grid,config);
	g = 1;
	int saut_ligne = 0;
	int saut_colonne = 0;
	while(g <= config->nbGen)
	{
		saut_colonne++;
		if(g%10 == 0)
		{
			saut_ligne++;
			saut_colonne = 0;
		}

		// Generate pixel data
		for (y=0 ; y<grid->size_y ; ++y)
		{
			for (x=0 ; x<grid->size_x ; ++x)
			{
				if(grid->t[x][y] == 1)
				{
					p[y+saut_ligne*grid->size_y][x+saut_colonne*grid->size_x][0] = g%4==0?0:255;
	            	p[y+saut_ligne*grid->size_y][x+saut_colonne*grid->size_x][1] = g%3==0?255:0;
					p[y+saut_ligne*grid->size_y][x+saut_colonne*grid->size_x][2] = g%2==0?250:150;
				}
				else
				{
					p[y+saut_ligne*grid->size_y][x+saut_colonne*grid->size_x][0] = 0;
	            	p[y+saut_ligne*grid->size_y][x+saut_colonne*grid->size_x][1] = 0;
					p[y+saut_ligne*grid->size_y][x+saut_colonne*grid->size_x][2] = 0;
				}
			}
		}

		jdv_nextGeneration(grid,config);
		g++;
	}

	// Write bitmap pixel data starting with the
	// bottom line of pixels, left hand side
	for (y=h-1 ; y>=0 ; --y)
	{
		for (x=0 ; x<w ; ++x)
		{
			// Write pixel components in BGR order
			fputc(p[y][x][2], f);
			fputc(p[y][x][1], f);
			fputc(p[y][x][0], f);
		}
	}

	// Close bitmap file
	fclose(f);
}
