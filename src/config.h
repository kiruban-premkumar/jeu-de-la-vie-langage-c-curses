#ifndef CONFIG_H
#define CONFIG_H
#define N 5 		// Default number of generation to show
#define YMAX 100 	// Maximum y size of the grid
#define XMAX 100 	// Maximum x size of the grid
#define YMIN 3 		// Minimum y size of the grid
#define XMIN 3 		// Minimum x size of the grid
#define HMAX 100 	// Maximum size of historique
#endif // CONFIG_H