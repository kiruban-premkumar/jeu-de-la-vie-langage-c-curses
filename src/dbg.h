// Macro for standard errors output.

#define log_err(M, ...) fprintf(stderr, "[ERROR] %s: " M "\n", argv[0], ##__VA_ARGS__)